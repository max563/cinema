package ru.nn.serg.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.nn.serg.dao.Film;
import ru.nn.serg.dao.FilmDB;
import ru.nn.serg.dao.FilmWrapper;
import ru.nn.serg.service.DBService;
import ru.nn.serg.service.FilmService;
import java.util.List;

@Controller
public class Controller1 {

    @Autowired
    FilmService filmService;

    @Autowired
    FilmWrapper filmWrapper;

    @Autowired
    DBService dbService;

    // Вводится (inject) из application.properties.
    @Value("${url.service}")
    private String urlService;

    @GetMapping("/")
    public String mainPage(Model model){
        String url=urlService;
        model.addAttribute("url",url);
        return "main_page";
    }

    @PostMapping("/save_url")
    public String saveUrl(@ModelAttribute("url") String url){
        urlService=url;
        return "main_page";
    }

    @PostMapping("/get_data")
    public String mainPageGetData(@ModelAttribute("url") String url, Model model){
        filmService.setUrl(url);
        filmWrapper.setFilmsList(filmService.getFilms());

        //dbService.findFilmsById(filmService.getIds());

        model.addAttribute("form",filmWrapper);
        return "main_page";
    }

    @PostMapping("/run")
    public String run(/*@ModelAttribute("filmsFromForm") ArrayList<FilmFromForm> filmsFromForm,*/
                    @ModelAttribute("form") FilmWrapper form,
                      Model model){
        List<Film> filmsList = form.getFilmsList();
        for(int x=0;x<filmsList.size(); x++){
            FilmDB filmDb=new FilmDB();
            filmDb.setId(Long.parseLong(filmsList.get(x).getId(),10));
            filmDb.setVr(filmsList.get(x).getVr());
            filmDb.setGenre(filmsList.get(x).getGenre());
            dbService.addFilm(filmDb);
        }

        System.out.println("");
        return "main_page";
    }

}
