package ru.nn.serg.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//Использование:
//@Autowired
//private FilmDbRepository filmDbRepository;
//filmDbRepository. ...

@Repository
public interface FilmDbRepository extends JpaRepository<FilmDB,Long> {
}
