package ru.nn.serg.dao;

import org.springframework.stereotype.Component;

@Component
public class SeanceProp {
    private String filmId;
    private String time;
    private String is3D;

    public SeanceProp(String filmId, String time, String is3D) {
        this.filmId = filmId;
        this.time = time;
        this.is3D = is3D;
    }
    public SeanceProp(SeanceProp sp){
        setFilmId(sp.getFilmId());
        setTime(sp.getTime());
        setIs3D(sp.getIs3D());
    }
    public SeanceProp(){}

    public String getFilmId() {
        return filmId;
    }

    public void setFilmId(String filmId) {
        this.filmId = filmId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIs3D() {
        return is3D;
    }

    public void setIs3D(String is3D) {
        this.is3D = is3D;
    }
}
