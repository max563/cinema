package ru.nn.serg.dao;

import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class Seances {

    //todo пока применяется TreeMap, данная коллекция сортированная, но не потокобезопасная
    //при возможности заменить на протокобезопасную коллекцию
    private Map<String, SeanceDate> seances= new TreeMap<>();

    public Map<String, SeanceDate> getSeances() {
        return seances;
    }

    public void setSeances(Map<String, SeanceDate> seances) {
        this.seances = seances;
    }

    public void add(String date, SeanceDate seanceDate){
        seances.put(date, seanceDate);
    }
}
