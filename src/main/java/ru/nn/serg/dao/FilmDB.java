package ru.nn.serg.dao;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class FilmDB {
    @Id
    private Long id;
    //Возрастной рейтинг
    private String vr;
    //Жанр
    private String genre;

    public FilmDB() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVr() {
        return vr;
    }

    public void setVr(String vr) {
        this.vr = vr;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
