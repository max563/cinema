package ru.nn.serg.dao;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SeanceDate {

    List<SeanceProp> seanceProps = new ArrayList<SeanceProp>();

    public SeanceDate(List<SeanceProp> seanceProps) {
        this.seanceProps = seanceProps;
    }

    public SeanceDate() {
    }

    public List<SeanceProp> getSeanceProps() {
        return seanceProps;
    }

    public void setSeanceProps(List<SeanceProp> seanceProps) {
        this.seanceProps = seanceProps;
    }

    public void add(SeanceProp sp) {
        seanceProps.add(sp);
    }

}
