package ru.nn.serg.dao;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Этот класс является оберткой для списка классов которые мы возвращаем
 * из формы в контроллер
 */
@Component
public class FilmWrapper {
    public List<Film> filmsList;

    public FilmWrapper(List<Film> filmsList) {
        this.filmsList = filmsList;
    }
    public FilmWrapper(){}

    public void addFilm (Film film){
        this.filmsList.add(film);
    }

    public List<Film> getFilmsList() {
        return filmsList;
    }

    public void setFilmsList(List<Film> filmsList) {
        this.filmsList = filmsList;
    }
}
