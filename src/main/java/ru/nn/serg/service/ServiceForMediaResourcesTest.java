package ru.nn.serg.service;

import org.junit.jupiter.api.Test;

import javax.script.ScriptException;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class ServiceForMediaResourcesTest {

    @Test
    void sendToVK() {
    }

    @Test
    void getPrice() throws ScriptException, NoSuchMethodException, IOException {
        String nameFilm="МУЛЬТ в кино", time="13:00", is3d="0", date="2020-04-23";
        ServiceForMediaResources service = new ServiceForMediaResources();
        int etalon = 100;
        int result = service.getPrice(nameFilm,time,is3d,date);
        assertEquals(etalon, result);
    }
}