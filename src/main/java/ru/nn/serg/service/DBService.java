package ru.nn.serg.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nn.serg.dao.FilmDB;
import ru.nn.serg.dao.FilmDbRepository;

import java.util.List;
import java.util.Optional;

@Service
public class DBService {

    @Autowired
    FilmDbRepository filmDbRepository;

    public List<FilmDB> list(){
        return filmDbRepository.findAll();
    }

    public void addFilm(FilmDB film){
        filmDbRepository. save(film);
    }

    /**
     * Ищет записи согласно предоставленных идентификаторов.
     * Порядок элементов в результате не гарантируется
     * @param ids Список идентификаторов
     * @return Список найденных записей
     */
    public List<FilmDB> findFilmsById(List<Long> ids){
        return filmDbRepository.findAllById(ids);
    }

    public Optional<FilmDB> findFilmById(Long id){
        return filmDbRepository.findById(id);
    }
}
