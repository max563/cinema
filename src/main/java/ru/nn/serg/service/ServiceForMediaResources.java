package ru.nn.serg.service;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import javax.script.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ServiceForMediaResources {

    //@org.springframework.beans.factory.annotation.Value("classpath:resources/CalculatePrice.js")
    //Resource resourceFile;

    public void sendToVK(){

    }

    /**
     * Получаем цену сеанса, здесь возвращаемым значением выбран int по причинетого что у цены не подразумевается копеек
     * а также с возвращаемым значением не будут проводится какие либо арифметические опперации
     * @return
     */
    public Integer getPrice(String nameFilm, String time, String is3d, String date) throws ScriptException, NoSuchMethodException, IOException {
//        ScriptEngine scriptEngine = new ScriptEngineManager().getEngineByName("JavaScript");
//        Bindings bindings=scriptEngine.createBindings();
//        bindings.put("nameFilm", nameFilm);
//        bindings.put("time", time);
//        bindings.put("is3d", is3d);
//        bindings.put("date", date);
//        Invocable invocable = (Invocable) scriptEngine;
//
//        Object result = invocable.invokeFunction("getPrice",bindings);

        //Value function = null;

        File file = new ClassPathResource("CalculatePrice.js").getFile();
        String res=new String(Files.readAllBytes(file.toPath()));


        //Resource str = resourceFile;
        Integer price=null;
        try (Context context = Context.create()) {
            Value function=context.eval("js", res);
            assert function.canExecute();
            price=function.execute(nameFilm,time,is3d,date).asInt();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Integer price= (Integer) result;
        return price;
    }
}
