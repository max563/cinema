package ru.nn.serg.service;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.nn.serg.dao.*;

import java.util.*;

@Service
public class FilmService {

    final RestTemplate restTemplate=new RestTemplate();
    private String resp;

    //@Value("${url.service}")
    private String url;
    //Если при разработке нужно получить фейковые данные о сеансах - установть в true
    boolean getFakeJsonData=true;

    @Autowired
    DBService dbService;

    @Autowired
    Seances seances;
   // @Autowired
    //SeanceDate seanceDate =new SeanceDate();
    //@Autowired
    //SeanceProp seanceProp = new SeanceProp();


    List<Film> films=new ArrayList<Film>();
    //List<Long> ids = new ArrayList<>();


    //Map<String, SeanceProp> seance=new HashMap<String, SeanceProp>();
    //List<Map<String, SeanceProp>> seances=new ArrayList<Map<String, SeanceProp>>();

//Читаем данные из json и добавляем недостающие данные из БД
    private void readFilms(){
        films.clear();
        JSONObject jo = null;
        try {
            jo = (JSONObject) new JSONParser().parse(getJsonData());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Получаеи информацию о Фильмах
        JSONObject filmsObj= (JSONObject) jo.get("films");

        JSONObject filmObj;
        for (Object key : filmsObj.keySet()) {
            Film film=new Film();
            String keyStr = (String) key;
            Long keyLong= Long.parseLong(keyStr,10);
            filmObj= (JSONObject) filmsObj.get(key);
            //System.out.println("" +keyStr + "=" + filmsObj.get(key));
            //ids.add(keyLong);
            //ищем совпадения в базе и устанавливаем недостающие поля из базы
            FilmDB tempFilm = dbService.findFilmById(keyLong).orElse(new FilmDB());

            film.setId(keyStr);
            film.setFr((String) filmObj.get("fr"));
            film.setVr(tempFilm.getVr());
            film.setGenre(tempFilm.getGenre());

            films.add(film);
            //System.out.println("");
        }

        //Получаем информацию о сеансах
        JSONObject seancesObj= (JSONObject) jo.get("seanses");
        JSONObject seanceDataObj;
        JSONObject SeancePropObj;
        for (Object dataKeyObj: seancesObj.keySet()){
            SeanceDate seanceDate =new SeanceDate();
            String dataStr = (String) dataKeyObj;
            JSONArray arrSeances = (JSONArray) seancesObj.get(dataKeyObj);
            Iterator iterArrSeances=arrSeances.iterator();
            while (iterArrSeances.hasNext()){
                JSONObject seancePropObj= (JSONObject) iterArrSeances.next();
                SeanceProp seanceProp = new SeanceProp();
                seanceProp.setFilmId((String) seancePropObj.get("f"));
                seanceProp.setTime((String) seancePropObj.get("t"));
                seanceProp.setIs3D((String) seancePropObj.get("is3d"));
                seanceDate.add(seanceProp);
            }
            seances.add(dataStr, seanceDate);
        }

        System.out.println("");
    }

    private void readSeances(){

    }

    private String getJsonData(){
        //В зависимости он настроек выше, отправляем подставной JSON либо реальный
        return getFakeJsonData?JsonMockData.data:restTemplate.getForObject(url,String.class);
    }

    public void getSeances(){ }

    public String getFilmNameById(String id){

        return "test Film";
    }

    public List<Film> getFilms(){
        readFilms();
        return films;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
