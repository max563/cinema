// noinspection BadExpressionStatementJS
(nameFilm, time, is3d, date)=>{
    let strToTimestamp = date+"T"+time+"+0300";
    //print("str: "+strToTimestamp);
    let dateTimeStamp = Date.parse(strToTimestamp);
    //print("dateTimeStamp: "+dateTimeStamp);
    let date_obj = new Date(dateTimeStamp);

    // день недели на основании даты
    let day_of_week = date_obj.getDay();

    function compare(time1,startTime,endTime) {
        let mStartTime = Date.parse(date + "T" + startTime+"+0300");
        let nEndTime = Date.parse(date + "T" + endTime+"+0300");
        //print(mStartTime+" "+time1+" "+nEndTime+" "+ret);
        return (time1 >= mStartTime) & (time1 <= nEndTime);
    }

    //Проверяем не МВК ли это
    if (nameFilm.includes("МУЛЬТ в кино")){
        return 100;
    }else {
        //Если выходной день
        if(day_of_week===6 || day_of_week===0){
            //print("enter to holiday");
            if(compare(dateTimeStamp,"08:00", "10:59")) {
                return 100;
            }else if (compare(dateTimeStamp,"11:00", "12:59")){
                return is3d==="1"?170:150;
            }else if (compare(dateTimeStamp,"13:00", "16:59")){
                return is3d==="1"?200:180;
            }else if (compare(dateTimeStamp,"17:00", "22:59")){
                return is3d==="1"?220:200;
            }else{
                return is3d==="1"?200:180;
            }
            //Если будний день
        }else {
            //print("enter to weekday");
            if (compare(dateTimeStamp,"08:00","10:59")){
                return 100;
            }else if (compare(dateTimeStamp,"11:00", "12:59")){
                return is3d==="1"?140:120;
            }else if (compare(dateTimeStamp,"13:00", "18:59")){
                return is3d==="1"?180:160;
            }else if (compare(dateTimeStamp,"19:00", "22:59")){
                return is3d==="1"?200:180;
            }else{
                return is3d==="1"?180:160;
            }
        }
    }
};